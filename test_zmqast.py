from itertools import cycle, count

from unittest import TestCase
import mock

import zmqast as mod


AF_INET = mod.netifaces.AF_INET
IPS = {
    'lo': {
        AF_INET: [{
            'addr': '127.0.0.1',
            'netmask': '255.0.0.0',
            'peer': '127.0.0.1'
        }],
    },
    'eth0': {
        AF_INET: [{
            'addr': '10.0.0.1',
            'broadcast': '10.0.0.255',
            'netmask': '255.255.255.0'
        }, {
            'addr': '192.168.1.1',
            'broadcast': '192.168.1.255',
            'netmask': '255.255.255.0'
        }]
    },
    'eth1': {
        AF_INET: [{
            'addr': '192.168.1.2',
            'broadcast': '192.168.1.255',
            'netmask': '255.255.255.0'
        }]
    },
    'wlan0': {
        AF_INET: [{
            'addr': '10.0.0.2',
            'broadcast': '10.0.0.255',
            'netmask': '255.255.255.0'
        }]
    }
}

NET = mock.Mock()
ZMQ = mock.Mock()
SOCK = mock.Mock()


@mock.patch.object(mod, 'netifaces', NET)
@mock.patch.object(mod, 'zmq', ZMQ)
@mock.patch.object(mod.time, 'sleep', mock.Mock())
@mock.patch.object(mod, 'socket', SOCK)
class MulticastTest(TestCase):

    def setUp(self):
        SOCK.reset_mock()

        ZMQ.reset_mock()
        ZMQ.Context().socket.side_effect = lambda *a, **k: mock.Mock(creation_args=(a, k))

        NET.reset_mock()

        NET.AF_INET = AF_INET
        NET.interfaces.return_value = IPS.keys()
        NET.ifaddresses.side_effect = IPS.get

    def test_ports_required(self):
        """PUB/SUB ports are required"""

        self.assertRaises(TypeError, mod.Multicast)
        self.assertRaises(TypeError, mod.Multicast, 123)
        self.assertRaises(TypeError, mod.Multicast, pub=123)
        self.assertRaises(TypeError, mod.Multicast, sub=123)

        m = mod.Multicast(123, 456)
        self.assertEqual(m.pub, 123)
        self.assertEqual(m.sub, 456)

    def test_defaults(self):
        """Sane defaults"""

        m = mod.Multicast(123, 456)

        self.assertEqual(m.group, '224.0.0.1')
        self.assertEqual(m.remember, 10)
        self.assertIsNone(m.prefix)
        self.assertTrue(m.retry)
        self.assertEqual(m.retry_timeout, 1)
        self.assertEqual(m.retry_multiplier, 2)
        self.assertEqual(m.max_retries, 10)
        self.assertEqual(m.interfaces, None)

    def test_get_local_ips(self):
        """Can get a list of all local IP addresses"""

        m = mod.Multicast(123, 456)
        ips = list(m.get_local_ips())

        self.assertEqual(len(ips), 4)
        self.assertIn('10.0.0.1', ips)
        self.assertIn('10.0.0.2', ips)
        self.assertIn('192.168.1.1', ips)
        self.assertIn('192.168.1.2', ips)

    def test_get_specific_nic(self):
        """Can get a list of all IP addresses for a single NIC"""

        m = mod.Multicast(123, 456, interfaces=['eth0'])
        ips = list(m.get_local_ips())

        self.assertEqual(len(ips), 2)
        self.assertIn('10.0.0.1', ips)
        self.assertIn('192.168.1.1', ips)

        m = mod.Multicast(123, 456, interfaces=['wlan0'])
        ips = list(m.get_local_ips())

        self.assertEqual(len(ips), 1)
        self.assertIn('10.0.0.2', ips)

    def test_get_specific_nics(self):
        """Can get a list of all IP addresses for multiple NICs"""

        m = mod.Multicast(123, 456, interfaces=['eth0', 'wlan0'])
        ips = list(m.get_local_ips())

        self.assertEqual(len(ips), 3)
        self.assertIn('10.0.0.1', ips)
        self.assertIn('10.0.0.2', ips)
        self.assertIn('192.168.1.1', ips)

    def test_override_nics(self):
        """Can override the list of NICs used"""

        m = mod.Multicast(123, 456, interfaces=['eth0', 'wlan0'])
        ips = list(m.get_local_ips('eth0'))

        self.assertEqual(len(ips), 2)
        self.assertIn('10.0.0.1', ips)
        self.assertIn('192.168.1.1', ips)

    def test_never_loop(self):
        """The loopback device is never used"""

        m = mod.Multicast(123, 456, interfaces=['lo'])
        ips = list(m.get_local_ips())

        self.assertEqual(len(ips), 0)

    def test_no_netifaces(self):
        """Will attempt to get at least one IP when netifaces is not installed"""

        ip = '192.168.2.4'
        with mock.patch.object(mod, 'netifaces', None):
            SOCK.socket().getsockname.return_value = (ip, 36954)

            m = mod.Multicast(123, 456)
            ips = list(m.get_local_ips())

            self.assertEqual(ips, [ip])

    def test_create_sockets(self):
        """Create a PUB/SUB socket pair for each IP address"""

        m = mod.Multicast(1, 2, group='239.192.0.1')
        pairs = m.create_sockets()

        self.assertIsInstance(pairs, dict)

        self.assertEqual(m.ctx.socket.call_count, 8,
                         'Two sockets should be created per IP')

        expected_pub_endpoints = [
            'epgm://{};239.192.0.1:1'.format(ip)
            for ip in m.get_local_ips()
        ]

        expected_sub_endpoints = [
            'epgm://{};239.192.0.1:2'.format(ip)
            for ip in m.get_local_ips()
        ]

        for sub, pub in pairs.items():
            self.assertTrue(pub.connect.called, 'Each pub socket must connect')
            for call in pub.connect.mock_calls:
                self.assertIn(call[1][0], expected_pub_endpoints,
                              'Each pub socket has an expected endpoint {}'.format(call[0]))

            self.assertTrue(sub.connect.called, 'Each sub socket must connect')
            for call in sub.connect.mock_calls:
                self.assertIn(call[1][0], expected_sub_endpoints,
                              'Each sub socket has an expected endpoint')

            self.assertIn(mock.call(
                ZMQ.IDENTITY, mock.ANY),
                sub.setsockopt.mock_calls,
                'Each sub socket receives an identity'
            )

            self.assertIn(mock.call(
                ZMQ.IDENTITY, mock.ANY),
                pub.setsockopt.mock_calls,
                'Each pub socket receives an identity'
            )

            self.assertIn(mock.call(sub, ZMQ.POLLIN), m.poll.register.mock_calls,
                                    'Each sub socket must be registered for polling')

    def test_prepare_pairs(self):
        """Socket pairs are created during preparation phase"""

        m = mod.Multicast(1, 2, group='239.192.0.1')

        self.assertIsInstance(m.pairs, dict)
        self.assertEqual(m.pairs, {})

        m.prepare()

        self.assertIsInstance(m.pairs, dict)
        self.assertNotEqual(m.pairs, {})

    def test_send(self):
        """Can send a message over every PUB socket"""

        m = mod.Multicast(1, 2)
        m.prepare()

        for pub in m.pairs.values():
            self.assertFalse(pub.send.called)

        m.send('foo')

        for pub in m.pairs.values():
            self.assertTrue(pub.send.called)

    def test_send_one(self):
        """Can send a message over one PUB socket"""

        m = mod.Multicast(1, 2)
        m.prepare()

        ctrl_sub, ctrl_pub = m.pairs.items()[0]

        for pub in m.pairs.values():
            self.assertFalse(pub.send.called)

        m.send('foo', pub=ctrl_pub)

        for pub in m.pairs.values():
            if pub == ctrl_pub:
                self.assertTrue(pub.send.called)
            else:
                self.assertFalse(pub.send.called)

    def test_send_with_prefix(self):
        """Can send messages with a prefix"""

        m = mod.Multicast(1, 2, prefix='DEMO')
        m.prepare()

        m.send('foo')

        for pub in m.pairs.values():
            pub.send.assert_called_with('DEMO foo')

    def test_send_with_other_prefix(self):
        """Can send messages with a different prefix"""

        m = mod.Multicast(1, 2, prefix='DEMO')
        m.prepare()

        m.send('foo', prefix='SAMPLE')

        for pub in m.pairs.values():
            pub.send.assert_called_with('SAMPLE foo')

    def test_recv(self):
        """Will wait to receive a message"""

        m = mod.Multicast(1, 2)
        m.prepare()

        ctrl_sub = m.pairs.keys()[0]
        ctrl_sub.recv.return_value = 'foo'

        m.poll.poll.side_effect = [[], [], [[ctrl_sub, ZMQ.POLLIN]],
                                   KeyboardInterrupt]

        out = m.recv()
        self.assertIsInstance(out, tuple)
        self.assertEqual(out[0], 'foo')
        self.assertEqual(out[1], ctrl_sub)

    def test_recv_specific(self):
        """Wait to receive a message on a specific socket"""

        m = mod.Multicast(1, 2)
        m.prepare()

        ctrl_sub, spec_sub = m.pairs.keys()[:2]
        ctrl_sub.recv.return_value = 'foo'

        m.poll.poll.side_effect = [[], [], [[ctrl_sub, ZMQ.POLLIN]],
                                   KeyboardInterrupt]

        # note that spec_sub is passed here, not ctrl_sub
        out = m.recv(sub=spec_sub, no_loop=True)
        self.assertIsNone(out)

    def test_recv_no_loop(self):
        """Attempt to receive a message once"""

        m = mod.Multicast(1, 2)
        m.prepare()

        ctrl_sub = m.pairs.keys()[0]
        ctrl_sub.recv.return_value = 'foo'

        m.poll.poll.side_effect = [[], [], [[ctrl_sub, ZMQ.POLLIN]],
                                   KeyboardInterrupt]

        out = m.recv(no_loop=True)
        self.assertIsNone(out)

    @mock.patch.object(mod.time, 'time')
    def test_req(self, time):
        """Can send a message and wait for a response"""

        m = mod.Multicast(1, 2)
        m.prepare()

        # simulate time passing
        c = count()
        time.side_effect = lambda: next(c)

        with mock.patch.object(m, 'send') as send, \
             mock.patch.object(m, 'recv') as recv:

            recv.side_effect = [None, None, None, ('bar', mock.Mock())]

            resp = m.req('foo')

            self.assertTrue(send.called)
            send.assert_called_with('foo', pub=None)

            self.assertTrue(recv.called)

        self.assertEqual(resp[0], 'bar')

    @mock.patch.object(mod.time, 'time')
    def test_timeout(self, time):
        """Will return None if all retries are exhausted"""

        m = mod.Multicast(1, 2, max_retries=5)
        m.prepare()

        # simulate time passing
        c = count()
        time.side_effect = lambda: next(c)

        with mock.patch.object(m, 'send') as send, \
             mock.patch.object(m, 'recv') as recv:

            recv.side_effect = cycle([None])

            self.assertIsNone(m.req('foo'))
            self.assertEqual(send.call_count, 6)

    def test_subscribe_one(self):
        """Can subscribe to a topic"""

        m = mod.Multicast(1, 2)

        with mock.patch.object(m, 'prepare') as prep:
            m.subscribe('foo')
            prep.assert_called_once_with()

        for sub, pub in m.pairs.items():
            sub.setsockopt.assert_has_call(ZMQ.SUBSCRIBE, 'foo')

    def test_subscribe_many(self):
        """Can subscribe to many topics"""

        m = mod.Multicast(1, 2)

        with mock.patch.object(m, 'prepare') as prep:
            m.subscribe('foo', 'bar', 'baz')
            prep.assert_called_once_with()

        for sub, pub in m.pairs.items():
            sub.setsockopt.assert_has_calls([
                mock.call(ZMQ.SUBSCRIBE, 'foo'),
                mock.call(ZMQ.SUBSCRIBE, 'bar'),
                mock.call(ZMQ.SUBSCRIBE, 'baz')
            ])

    def test_unsubscribe_one(self):
        """Can unsubscribe from a topic"""

        m = mod.Multicast(1, 2)
        m.prepare()

        m.unsubscribe('foo')

        for sub, pub in m.pairs.items():
            sub.setsockopt.assert_has_call(ZMQ.UNSUBSCRIBE, 'foo')

    def test_unsubscribe_many(self):
        """Can unsubscribe from many topics"""

        m = mod.Multicast(1, 2)
        m.prepare()

        m.unsubscribe('foo', 'bar', 'baz')

        for sub, pub in m.pairs.items():
            sub.setsockopt.assert_has_calls([
                mock.call(ZMQ.UNSUBSCRIBE, 'foo'),
                mock.call(ZMQ.UNSUBSCRIBE, 'bar'),
                mock.call(ZMQ.UNSUBSCRIBE, 'baz')
            ])

    def test_parse_cb(self):
        """Can specify a custom parse function"""

        fake = mock.Mock()
        m = mod.Multicast(1, 2, parse_cb=fake)
        m.prepare()

        ctrl_sub = m.pairs.keys()[0]
        ctrl_sub.recv.return_value = 'foo'

        m.poll.poll.side_effect = [[], [], [[ctrl_sub, ZMQ.POLLIN]],
                                   KeyboardInterrupt]

        m.recv()

        fake.assert_called_once_with('foo')

    def test_recv_attempts(self):
        """Can break out of recv loop prematurely"""

        m = mod.Multicast(1, 2)
        m.prepare()

        m.poll.poll.side_effect = [[], [], [], [], []]

        out = m.recv(attempts=4)

        self.assertEqual(m.poll.poll.call_count, 4)
        self.assertIsNone(out)


@mock.patch.object(mod, 'netifaces', NET)
@mock.patch.object(mod, 'zmq', ZMQ)
@mock.patch.object(mod.time, 'sleep', mock.Mock())
@mock.patch.object(mod, 'socket', SOCK)
class ServerTest(TestCase):

    def setUp(self):
        SOCK.reset_mock()

        ZMQ.reset_mock()
        ZMQ.Context().socket.side_effect = lambda *a, **k: mock.Mock(creation_args=(a, k))

        NET.reset_mock()

        NET.AF_INET = AF_INET
        NET.interfaces.return_value = IPS.keys()
        NET.ifaddresses.side_effect = IPS.get

    def test_break(self):
        """Can break out of a server loop"""

        s = mod.Server(1, 2)
        s.prepare()

        def handle(msg, pub, sub, server):
            server.terminate()

        with mock.patch.object(s, 'recv') as recv:
            recv.side_effect = [('bar', s.pairs.keys()[0])]

            self.assertFalse(s.terminated)
            s.listen(handle)
            self.assertTrue(s.terminated)
