from setuptools import setup

from zmqast import __version__


setup(
    name='zmqast',
    version=__version__,
    author='Josh VanderLinden',
    author_email='codekoala@gmail.com',
    description='Multicast client/server helpers for Python and ZeroMQ',
    license='BSD',
    url='https://bitbucket.org/codekoala/zmq-multicast',
    py_modules=['zmqast'],
    zip_safe=True,
    test_suite='test_zmqast'
)
