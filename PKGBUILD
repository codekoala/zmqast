# Contributor: Josh VanderLinden <arch@cloudlery.com>

_base=zmqast
pkgname=(python2-${_base} python-${_base})
pkgver=0.6
pkgrel=1
pkgdesc="Multicast client/server helpers for Python and ZeroMQ"
url="https://bitbucket.org/codekoala/${_base}/overview"
license=('BSD')
arch=('any')
source=(
  "./zmqast.py"
  "./test_zmqast.py"
  "./LICENSE"
)
md5sums=('3c0acab3fa33638e3870b231164a0530'
         'a857c203a7003848b0d1e769373bd4f1'
         '5ba0f5b314843d6b2b3e8787478f0ff6')
checkdepends=('python2-mock')

check() {
  cd ..

  python2 setup.py test
}

pkgver() {
  python2 -c "from zmqast import __version__; print(__version__)"
}

py-install() {
  local py=$1
  depends=("${py}" "${py}-pyzmq")
  optdepends=(
    "${py}-netifaces: better IP address detection"
  )

  cd ..

  ${py} setup.py install --optimize=1 --root="$pkgdir"

  install -Dm644 LICENSE ${pkgdir}/usr/share/licenses/${pkgname}/LICENSE
}

package_python-zmqast() {
  py-install python
}

package_python2-zmqast() {
  py-install python2
}

# vim:et ts=2 sw=2:
