from collections import deque
from itertools import count
import argparse
import logging
import socket
import sys
import time

try:
    import netifaces
except ImportError:
    netifaces = None

import zmq


__version__ = '0.6'


GROUP = '224.0.0.1'


class Multicast(object):

    def __init__(self, pub, sub, group=None, remember=10, prefix=None,
                 retry=True, retry_timeout=1, retry_multiplier=2,
                 max_retries=10, interfaces=None, parse_cb=None):
        """
        Create a multicast client or server.

        * ``pub``: port to publish messages to
        * ``sub``: port to listen for messages on
        * ``group``: multicast group to connect to. Defaults to ``224.0.0.1``,
          which includes all devices on the current network segment.
        * ``remember``: the number of incoming messages to remember in order to
          avoid duplication. Default: ``10``
        * ``prefix``: the prefix to send on all outbound messages. Servers
          should probably have no prefix, while clients should have a prefix
          for identifications purposes. Optional.
        * ``retry``: whether or not to retry sending messages when a response
          is not immediately received. Default: ``True``
        * ``retry_timeout``: number of seconds to wait before retrying the
          first time. Default: ``1``
        * ``retry_multiplier``: multiplier for subsequent retries, to avoid
          spamming the network. For example, if you set ``retry_timeout`` to
          ``1`` and ``retry_multiplier`` to ``2`` (default), we would wait
          ``1`` second before the first retry, ``2`` seconds before the next
          retry, ``4`` seconds before the next, ``8`` seconds before the next,
          and so on.
        * ``max_retries``: maximum number of times to retry sending before
          aborting. Default: ``10``
        * ``interfaces``: the names of each network interface to return IP
          addresses for. If not specified, all network interfaces are used.
        * ``parse_cb``: an optional callback function to parse an incoming
          message. The callback function must accept the same parameters as the
          ``parse`` method in this class.

        """

        self.log = logging.getLogger(self.__class__.__name__)

        self.pub = pub
        self.sub = sub
        self.group = group or GROUP
        self.remember = remember
        self.prefix = prefix
        self.retry = retry
        self.retry_timeout = retry_timeout
        self.retry_multiplier = retry_multiplier
        self.max_retries = max_retries
        self.interfaces = interfaces

        # replace the parse function with a custom callback
        if callable(parse_cb):
            self.parse = parse_cb

        self.ctx = zmq.Context()
        self.poll = zmq.Poller()
        self.pairs = {}
        self.recent = deque(maxlen=self.remember)

    def subscribe(self, *topics):
        """
        Subscribe to messages for the specified topic(s).

        * ``topics``: the topic(s) to subscribe to.

        """

        self.prepare()

        for sub in self.pairs:
            for topic in topics:
                self.log.info('Subscribing to "{}" on {}'.format(
                    topic, sub.getsockopt(zmq.IDENTITY)
                ))
                sub.setsockopt(zmq.SUBSCRIBE, topic)

    def unsubscribe(self, *topics):
        """
        Unsubscribe from messages for the specified topic(s).

        * ``topics``: the topic(s) to unsubscribe from.

        """

        self.prepare()

        for sub in self.pairs:
            for topic in topics:
                sub.setsockopt(zmq.UNSUBSCRIBE, topic)

    def get_local_ips(self, interfaces=None):
        """
        Generator of local addresses (excluding loopback).

        You may limit the results of this function by specifying a list of
        interface names. You may also specify this list of interface names
        when instantiating the class using the ``interfaces`` parameter.

        * ``interfaces``: a list of interface names to include IPs for. This
          parameter is ignored if the *netifaces* library is not available.

        """

        if netifaces is not None:
            if interfaces is None:
                interfaces = self.interfaces

            for nic in netifaces.interfaces():
                if nic == 'lo':
                    continue

                if interfaces is not None and nic not in interfaces:
                    self.log.warn('Skipping NIC: {}'.format(nic))
                    continue

                addrs = netifaces.ifaddresses(nic)
                for addr in addrs.get(netifaces.AF_INET, []):
                    if addr['addr'] == '127.0.0.1':
                        continue

                    yield addr['addr']
        else:
            self.log.warn('netifaces not available; using fallback to get IP addresses')
            used = set()
            for dest in ('8.8.8.8', self.group):
                try:
                    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    s.connect((dest, 80))
                except socket.error:
                    continue
                else:
                    ip = s.getsockname()[0]
                    if ip not in used:
                        yield ip

                    used.add(ip)
                finally:
                    # TODO: handle an exception?
                    s.close()

    def create_sockets(self):
        """
        Return a dictionary of {sub: pub} sockets for all local IPs.

        You may limit the sockets created by specifying a list of interface
        names when instaniating the object.
        """

        pairs = {}

        for ip in self.get_local_ips():
            self.log.info('Waiting for peers on {}'.format(ip))
            endpoint = 'epgm://{};{}:{{}}'.format(ip, self.group)

            pub_end = endpoint.format(self.pub)
            pub = self.ctx.socket(zmq.PUB)
            pub.setsockopt(zmq.IDENTITY, 'PUB:{}'.format(ip))
            pub.connect(pub_end)

            sub_end = endpoint.format(self.sub)
            sub = self.ctx.socket(zmq.SUB)
            sub.setsockopt(zmq.IDENTITY, 'SUB:{}'.format(ip))
            sub.connect(sub_end)

            self.poll.register(sub, zmq.POLLIN)

            pairs[sub] = pub

            self.log.debug('PUB: {}'.format(pub_end))
            self.log.debug('SUB: {}'.format(sub_end))

        return pairs

    def prepare(self):
        """
        Additional preparations before commencing server/client duties.

        Normally this would be done in the ``__init__`` method, but socket
        creation is better left for later. Hence this method.

        """

        if not self.pairs:
            self.pairs = self.create_sockets()
            self.log.info('Done preparing sockets')

    def send(self, msg, pub=None, prefix=None):
        """
        Send a prefixed message over all PUB sockets.

        By default, this method will attempt to send ``msg`` over all PUB
        sockets. If you wish to send the message on a specific socket, specify
        which socket using the ``pub`` parameter.

        If you specified a ``prefix`` when instantiating the class, ``msg``
        will be prefixed with that value and a space.

        * ``msg``: the message to send.
        * ``pub``: a specific PUB socket to send the message over. Optional.
        * ``prefix``: an identifier for one or more subscribers. Optional.

        """

        if pub is None:
            socks = list(self.pairs.values())
        else:
            socks = [pub]

        if prefix is None:
            prefix = self.prefix

        if prefix:
            out = '{} {}'.format(prefix, msg)
        else:
            out = msg

        for sock in socks:
            self.log.debug('Sending on {}: {}'.format(sock.getsockopt(zmq.IDENTITY), out))
            sock.send(out)

    def recv(self, sub=None, no_loop=False, attempts=None):
        """
        Receive a message from any SUB socket.

        By default, this method will attempt to receive messages on any SUB
        socket. If you wish to wait for a message coming in from a specific SUB
        socket, specify which socket using the ``sub`` parameter.

        If you wish to attempt a single message retrieval, set ``no_loop`` to
        True. If a message is available on any SUB socket, that will be
        returned. Returns ``None`` otherwise.

        Be aware that any message you receive from this may not be the message
        you expect if multiple SUB sockets are receiving messages concurrently.
        The ordering of the messages is non-deterministic.

        When a message is available, it will be returned as the first item in a
        tuple. The second item will be the SUB socket that the message arrived
        on.

        * ``sub``: the specific SUB socket you wish to wait for a message on.
          Optional.
        * ``no_loop``: whether or not to wait until a message is available. If
          set to ``True``, only check for a message once and return.
        * ``attempts``: the number of times to poll for messages.

        """

        if no_loop:
            attempts = 1

        try:
            attempts = int(attempts)
        except TypeError:
            keep_looping = lambda: True
        else:
            c = count()
            keep_looping = lambda: next(c) < attempts

        while keep_looping():
            socks = dict(self.poll.poll(1000))
            for incoming in socks:
                if sub and incoming != sub:
                    continue

                msg = self.parse(incoming.recv().decode())
                if msg in self.recent:
                    self.log.debug('Duplicate message: {}'.format(msg))
                    continue

                self.recent.append(msg)

                return (msg, incoming)

    def req(self, msg, pub=None, retry=None, retry_timeout=None,
            retry_multiplier=None, max_retries=None):
        """
        Send a message and wait for a response.

        This is an approximation of a REQ/REP socket pair. The message will be
        sent over all PUB sockets by default. To specify a specific PUB socket,
        use the ``pub`` parameter.

        After sending the message, all SUB sockets are checked for a message
        once every half second until one arrives or the current retry timeout
        is reached. After each retry, the retry timeout is multiplied by the
        ``retry_multiplier``, which is ``2`` by default. If no response can be
        retrieved from any SUB socket after ``max_retries``, return None.

        This method will send at most ``max_retries + 1`` copies of ``msg``.

        * ``msg``: the message to send.
        * ``pub``: the specific PUB socket to send the message over. Optional.
        * ``retry``: whether or not to retry sending ``msg``. Default: ``True``
        * ``retry_timeout``: the initial retry interval in seconds. Default: 1
        * ``retry_multiplier``: the amount to multiply the retry timeout after
          each timeout interval. Default: 2 (double the timeout after each
          failure)
        * ``max_retries``: maximum number of times to retry sending ``msg``.

        """

        rep = None
        retry = retry or self.retry
        timeout = retry_timeout or self.retry_timeout
        multiplier = retry_multiplier or self.retry_multiplier
        max_retries = max_retries or self.max_retries

        if not retry:
            self.send(msg, pub=pub)
            rep = self.recv()
        else:
            retries = 0
            while True:
                if retries:
                    self.log.debug('Retry #{}'.format(retries))

                self.send(msg, pub=pub)

                mark = time.time() + timeout
                while time.time() <= mark:
                    rep = self.recv(no_loop=True)
                    if rep:
                        break

                    time.sleep(0.5)

                if rep is not None or retries >= max_retries:
                    break
                else:
                    timeout *= multiplier
                    retries += 1

        return rep

    def parse(self, msg):
        """
        Parse an incoming message into an expected format.

        * ``msg``: the message as it arrived over the socket.

        """

        return msg


class Server(Multicast):

    def __init__(self, *args, **kwargs):
        super(Server, self).__init__(*args, **kwargs)

        self._terminate_flag = False

    def listen(self, handler):
        """
        Wait for messages from peers.

        By default, this method will act as an echo server. Any message that
        arrives will simply be sent back out with `` ECHO`` appended to it.
        Customize the message handling by specifying your own ``handler``
        method. The method should accept three parameters:

        * ``msg``: the message that just arrived.
        * ``pub``: the PUB socket associated with the incoming message.
        * ``sub``: the SUB socket that the message arrived on.
        * ``server``: the Server instance that received the message.

        This method automatically remembers the messages that arrived most
        recently. This is to avoid duplicating work should the same message
        arrive on multiple sockets or if it's broadcast more than once.

        """

        self.prepare()

        try:
            while not self.terminated:
                got = self.recv()
                if not got:
                    continue

                msg, incoming = got
                outgoing = self.pairs.get(incoming, None)
                if outgoing is None:
                    self.log.error('Unknown socket pair')
                    continue

                handler(msg, outgoing, incoming, self)
        except KeyboardInterrupt:
            pass
        finally:
            self.log.info('Terminating')

    def terminate(self):
        """Flag the server loop to finish"""

        self._terminate_flag = True

    @property
    def terminated(self):
        return self._terminate_flag


class EchoServer(Server):

    def echo(self, msg, pub, sub, server):
        """
        Example message handler.

        Simply echo any message back over the associated PUB socket.

        """

        self.send('{} ECHO'.format(msg), pub=pub)


class EchoClient(Multicast):

    def echo(self):
        """
        Example client.

        Send messages to peers and expect it to be echoed.

        """

        self.prepare()

        count = 0

        try:
            while True:
                resp = self.req('demo {}'.format(count))
                if resp is None:
                    self.log.error('Failed to receive response')
                    continue

                resp, incoming = resp
                outgoing = self.pairs.get(incoming, None)
                if outgoing is None:
                    self.log.error('Unknown socket pair')
                    continue

                self.log.debug('Received on {}: {}'.format(
                    incoming.getsockopt(zmq.IDENTITY), resp
                ))

                time.sleep(1)
                count += 1
        except KeyboardInterrupt:
            self.log.info('Terminating')


def main():
    cli = argparse.ArgumentParser()
    cli.add_argument('prefix', nargs='?',
                     help='Subscription prefix')
    cli.add_argument('-i', '--interface', nargs='+',
                     help='Specific interface to listen on')
    cli.add_argument('-p', '--pub', type=int, default=5000,
                     help='Port to publish on')
    cli.add_argument('-s', '--sub', type=int, default=5001,
                     help='Port to subscribe on')

    opts = cli.parse_args()

    logging.basicConfig(
        format='%(asctime)s %(levelname)-6s %(message)s',
        datefmt='%m/%d %H:%M:%S',
        level=logging.DEBUG,
        stream=sys.stdout
    )

    if opts.prefix:
        print('Running as client')
        m = EchoClient(opts.sub, opts.pub, prefix=opts.prefix,
                            interfaces=opts.interface)
        m.subscribe(opts.prefix)
        m.echo()
    else:
        print('Running as server')
        m = EchoServer(opts.pub, opts.sub, interfaces=opts.interface)
        m.subscribe('')
        m.listen(m.echo)


if __name__ == '__main__':
    main()
