======
zmqast
======

This is a library to simplify creating ZeroMQ-based multicast clients and
servers.

Dependencies
============

``zmqast`` depends on:

* PyZMQ. Tested with PyZMQ 14.0.0 for Python 2.7 on Linux and Windows 7 and 8.
  Note that a special build of PyZMQ has to be installed on Windows, as the
  default installers/packages don't include PGM/EPGM socket support.

  See https://github.com/zeromq/pyzmq/issues/422#issuecomment-28679307 for
  steps to build the Windows installer. This is usually not an issue on Linux
  boxes.
* ``netifaces``. If available, it will be used to detect the IP addresses on
  the host system. If not available, a fallback is in place to attempt to find
  an IP address.

Usage
=====

This library comes with a sample client/server pair. The server will simply
echo what the client(s) send. To start the server, run the following command::

    python -mzmqast

To run a client, run::

    python -mzmqast foo

with ``foo`` being the identifier for that particular client. The client will
subscribe to any published message that begins with ``foo``.

.. important::

    You generally can't have a multicast server and a multicast client running
    on the same machine at the same time. If you're trying to run the above
    commands on the same machine and it's not working, try running each on
    different machines on the same network.

Testing
=======

You can run the unit tests for this library with::

    python setup.py test

Reporting Bugs
==============

If you find a bug or have other feedback, please create an issue on the
official issue tracker at:

    https://bitbucket.org/codekoala/zmqast/issues
